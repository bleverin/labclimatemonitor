<?php
// Pull in the NuSOAP code
require_once('/usr/share/php/nusoap/nusoap.php');
// Create the server instance
$server = new soap_server;
// Register the method to expose
$server->register('hello');
// Define the method as a PHP function
function hello($name) {
    return 'Hello, ' . $name;
}


// Register the method to expose
$server->register('InsertTx5xxSample');

function InsertTx5xxSample($passKey,$device,$temp,$relHum,$compQuant,$pressure,$alarms,$compType,$tempU,$pressureU,$timer) {
$data = Time() ." ". $temp ." ". $relHum  ." ". $pressure ." ".  $alarms  ." ". $device  ." ". date("d/m/Y")  ." (unixtime/temp/relH/press/alarms/device/date)  \n";
$file_write = fopen("soaplog/soaplog2.log", "a");
fwrite($file_write, $data);
fclose($file_write);
//return 'Hello,' . $passkey;
}
// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>
