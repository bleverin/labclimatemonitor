{
  gStyle->SetOptStat(0);

  ifstream soapfile;

  soapfile.open("/home/soap/public_html/soap/soaplog/soaplog2.log");

  ifstream weatherfile;

  weatherfile.open("/home/soap/public_html/soap/heidelbergweather.txt");

  Int_t unixtime;
  Float_t temperature, humidity, pressure;
  Float_t temperature_max=-1000.0;
  Float_t temperature_min = +1000.0;
  Int_t unixtime_w;
  Float_t temperature_w, humidity_w, pressure_w;
  Float_t ppm_w, gm3_w, precip_w;
  string alarms;
  Int_t device;
  string date;
  string clocktime;
  string unit;
  string weather;

  TFile *f;
   TTree *tree;
   f = new TFile("soaplog.root","RECREATE");
   tree = new TTree("T","TempRhPressureSensor");
   tree->Branch("unixtime",&unixtime,"unixtime/I");
   tree->Branch("temperature",&temperature,"temperature/F");
   tree->Branch("humidity",&humidity,"humidity/F");
   tree->Branch("pressure",&pressure,"pressure/F");



   Int_t nlines = 0;
   Int_t firsttime = 0;
  while(true){

    soapfile >> unixtime >> temperature >> humidity >> pressure >> alarms >> device >> date >> unit;
    if (!soapfile.good()) break; 
    if(nlines==0) firsttime = unixtime;
    if(temperature>temperature_max) temperature_max = temperature;
    if(temperature<temperature_min) temperature_min = temperature;

    date+=" "+ clocktime;
    //  cout << unixtime <<" " << temperature <<" " << humidity <<" " << pressure <<" " << alarms <<" " << device <<" " << date<<" " << unit << endl;
    tree->Fill();
    nlines++;
  }
  soapfile.close();
  f->Write();
  f->Close();


   TFile *f_w;
   TTree *tree_w;
   f_w = new TFile("soaplog_weather.root","RECREATE");
   tree_w = new TTree("T_w","TempRhPressure_weather");
   tree_w->Branch("unixtime_w",&unixtime_w,"unixtime_w/I");
   tree_w->Branch("temperature_w",&temperature_w,"temperature_w/F");
   tree_w->Branch("humidity_w",&humidity_w,"humidity_w/F");
   tree_w->Branch("pressure_w",&pressure_w,"pressure_w/F");
   tree_w->Branch("ppm_w",&ppm_w,"ppm_w/F");
   tree_w->Branch("gm3_w",&gm3_w,"gm3_w/F");
   tree_w->Branch("precip_w",&precip_w,"precip_w/F");




  while(true){
    weatherfile >> unixtime_w >> temperature_w >> humidity_w >> pressure_w >> ppm_w >> gm3_w >> precip_w >> clocktime >> unit >> weather;
       if (!weatherfile.good()) break; 
  // if(nlines==0) firsttime = unixtime;
  //  date+=" "+ clocktime;
       //  cout << unixtime_w <<" " << temperature_w <<" " << humidity_w <<" " << pressure_w <<" " << unit << endl;
    tree_w->Fill();
    //    nlines++;
  }
  weatherfile.close();
  f_w->Write();
  f_w->Close();
  
  
  TFile *fopen;
  // gROOT->ProcessLine(".L lhcbStyle.C");
  //   lhcbStyle();
  

 fopen = new TFile("soaplog.root","OPEN");
 
 //monthly

 TCanvas* c1 = new TCanvas("c1", "Lab Conditions", 800, 800);
 c1->Divide(1,2);
 c1_1->cd();
 TH2F * h2temp = new TH2F("h2temp","Lab Temperature (#circC)",nlines,unixtime-2419200,unixtime,60,19.0,25.0);
 T->Project("h2temp","temperature:unixtime");
 h2temp->GetXaxis()->SetTimeDisplay(1);
 h2temp->GetXaxis()->SetTimeOffset(0,"gmt");
 h2temp->GetXaxis()->SetTimeFormat("%d/%m");
 h2temp->GetXaxis()->SetTitle("Date");
 //h2temp->GetYaxis()->SetTitle("Temperature (#circC)");
 Double_t meantemp = h2temp->GetMean(2);
 TLine * lmeantemp = new TLine(unixtime-2419200,meantemp,unixtime,meantemp);
 TLine * lmeantempup = new TLine(unixtime-2419200,temperature_max,unixtime,temperature_max);
 TLine * lmeantempdown = new TLine(unixtime-2419200,temperature_min,unixtime,temperature_min);
 lmeantemp->SetLineColor(kBlue);lmeantempup->SetLineColor(kRed);lmeantempdown->SetLineColor(kRed);
 h2temp->SetMarkerStyle(kFullDotMedium);

 h2temp->Draw("");
TH2F * h2tempcopy = (TH2F*)h2temp->Clone("h2tempcopy");
 h2tempcopy->GetXaxis()->SetTimeDisplay(1);
 h2tempcopy->GetXaxis()->SetTimeOffset(0,"gmt");
 h2tempcopy->GetXaxis()->SetTimeFormat("%Y");
 h2tempcopy->GetXaxis()->SetLabelOffset(0.05);
 TPad * c1_1copy = (TPad*)c1_1->Clone("c1_1copy");
 c1_1copy->SetFillStyle(4000);
 c1->cd();
 c1_1copy->Draw();
 c1_1copy->cd();
 h2tempcopy->Draw("AXIS");
 
 h2temp->Draw("same");//lmeantemp->Draw("same");lmeantempup->Draw("same");lmeantempdown->Draw("same");

 c1_2->cd();
 gStyle->SetOptStat(1);
 c1_2->cd();
 h2temp->ProjectionY();
 h2temp_py->Draw();
 // c1_hist->SaveAs("temp_month_hist.png");
 //c1_hist->SaveAs("temp_month_hist.pdf");
 c1_2->Update();
 TCanvas *c1_2_2 = new TCanvas ("c1_2_2","c1_2",800,800);
 c1_2_2->Divide(1,2);
 TImage *img = TImage::Create();
 img->FromPad(c1_2);
 c1_2_2->cd(1);
 img->Flip(90);
 img->Draw("x");
 c1_2_2->Update();
 c1_2_2->cd(2);
 img->Flip(90);
 img->Draw("x");
 c1_2_2->SaveAs("humi_month_hist.png");
 c1_2_2->SaveAs("humi_month_hist.pdf");


 /*
 c1_2->cd();
 TH2F * h2humi = new TH2F("h2humi","Lab Rel. Humidity (\%)",nlines,unixtime-2419200,unixtime,100,5.0,55.0);
 T->Project("h2humi","humidity:unixtime");
 h2humi->GetXaxis()->SetTimeDisplay(1);
h2humi->GetXaxis()->SetTimeOffset(0,"gmt");
 h2humi->GetXaxis()->SetTimeFormat("%d/%m");
 h2humi->GetXaxis()->SetTitle("Date");
 h2humi->GetXaxis()->SetLabelOffset(0.01);
 h2humi->SetMarkerStyle(kFullDotMedium);

 h2humi->Draw("");

 TH2F * h2humicopy = (TH2F*)h2humi->Clone("h2humicopy");
 h2humicopy->GetXaxis()->SetTimeDisplay(1);
 h2humicopy->GetXaxis()->SetTimeOffset(0,"gmt");
 h2humicopy->GetXaxis()->SetTimeFormat("%Y");
 h2humicopy->GetXaxis()->SetLabelOffset(0.05);
 TPad * c1_2copy = (TPad*)c1_2->Clone("c1_2copy");
 c1_2copy->SetFillStyle(4000);
 c1->cd();
 c1_2copy->Draw();
 c1_2copy->cd();
 h2humicopy->Draw("AXIS");
 h2humi->Draw("same");

 c1->Update();
 c1->SaveAs("labconditions_month.png");
 c1->SaveAs("labconditions_month.pdf");

 
TCanvas* c1_hist = new TCanvas("c1_hist", "Lab Conditions", 800, 800);
 gStyle->SetOptStat(1);
 c1_hist->Divide(1,2);
 c1_hist_1->cd();
 h2temp->ProjectionY();
 h2temp_py->Draw();
 // c1_hist->SaveAs("temp_month_hist.png");
 //c1_hist->SaveAs("temp_month_hist.pdf");
 c1_hist_2->cd();
 h2humi->ProjectionY();
 h2humi_py->Draw();
 c1_hist->SaveAs("humi_month_hist.png");
 c1_hist->SaveAs("humi_month_hist.pdf");
 */

 //fopen.Close();

 //yearly


 TCanvas* c1year = new TCanvas("c1year", "Lab Conditions", 800, 800);
 gStyle->SetOptStat(0);
 c1year->Divide(1,2);
 c1year_1->cd();
 TH2F * h2tempyear = new TH2F("h2tempyear","Lab Temperature (#circC)",365,unixtime-31556926,unixtime,60,19.0,25.0);
 T->Project("h2tempyear","temperature:unixtime");
 h2tempyear->GetXaxis()->SetTimeDisplay(1);
 h2tempyear->GetXaxis()->SetTimeOffset(0,"gmt");
 h2tempyear->GetXaxis()->SetTimeFormat("%d/%m");
 h2tempyear->GetXaxis()->SetTitle("Date");
 //h2tempyear->GetYaxis()->SetTitle("Temperature (#circC)");
 Double_t meantemp = h2tempyear->GetMean(2);
 // TLine * lmeantemp = new TLine(unixtime-31556926,meantemp,unixtime,meantemp);
 //TLine * lmeantempup = new TLine(unixtime-31556926,temperature_max,unixtime,temperature_max);
 //TLine * lmeantempdown = new TLine(unixtime-31556926,temperature_min,unixtime,temperature_min);
 lmeantemp->SetLineColor(kBlue);lmeantempup->SetLineColor(kRed);lmeantempdown->SetLineColor(kRed);
 h2tempyear->SetMarkerStyle(kFullDotMedium);

 h2tempyear->Draw("");
 h2tempyear->ProfileX();
 TH2F * h2tempyearcopy = (TH2F*)h2tempyear->Clone("h2tempyearcopy");
 h2tempyearcopy->GetXaxis()->SetTimeDisplay(1);
 h2tempyearcopy->GetXaxis()->SetTimeOffset(0,"gmt");
 h2tempyearcopy->GetXaxis()->SetTimeFormat("%Y");
 h2tempyearcopy->GetXaxis()->SetLabelOffset(0.05);
 TPad * c1year_1copy = (TPad*)c1year_1->Clone("c1year_1copy");
 c1year_1copy->SetFillStyle(4000);
 c1year->cd();
 c1year_1copy->Draw();
 c1year_1copy->cd();
 h2tempyearcopy->Draw("AXIS");
 
 h2tempyear_pfx->Draw("same");//lmeantemp->Draw("same");lmeantempup->Draw("same");lmeantempdown->Draw("same");

 c1year_2->cd();
 TH2F * h2humiyear = new TH2F("h2humiyear","Lab Rel. Humidity (\%)",365,unixtime-31556926,unixtime,100,5.0,55.0);
 T->Project("h2humiyear","humidity:unixtime");
 h2humiyear->GetXaxis()->SetTimeDisplay(1);
h2humiyear->GetXaxis()->SetTimeOffset(0,"gmt");
 h2humiyear->GetXaxis()->SetTimeFormat("%d/%m");
 h2humiyear->GetXaxis()->SetTitle("Date");
 h2humiyear->GetXaxis()->SetLabelOffset(0.01);
 h2humiyear->SetMarkerStyle(kFullDotMedium);

 h2humiyear->Draw("");
 h2humiyear->ProfileX();
 TH2F * h2humiyearcopy = (TH2F*)h2humiyear->Clone("h2humiyearcopy");
 h2humiyearcopy->GetXaxis()->SetTimeDisplay(1);
 h2humiyearcopy->GetXaxis()->SetTimeOffset(0,"gmt");
 h2humiyearcopy->GetXaxis()->SetTimeFormat("%Y");
 h2humiyearcopy->GetXaxis()->SetLabelOffset(0.05);
 TPad * c1year_2copy = (TPad*)c1year_2->Clone("c1year_2copy");
 c1year_2copy->SetFillStyle(4000);
 c1year->cd();
 c1year_2copy->Draw();
 c1year_2copy->cd();
 h2humiyearcopy->Draw("AXIS");
 h2humiyear_pfx->Draw("same");

 c1year->Update();
 c1year->SaveAs("labconditions_1year.png");
 c1year->SaveAs("labconditions_1year.pdf");


 TCanvas* c1year_hist = new TCanvas("c1year_hist", "Lab Conditions", 800, 800);
 c1year_hist->Divide(1,2);
 c1year_hist_1->cd();
 gStyle->SetOptStat(1);
 h2tempyear->ProjectionY();
 h2tempyear_py->Draw();
 //c1year_hist->SaveAs("year_hist.png");
 //c1year_hist->SaveAs("year_hist.pdf");
 h2humiyear->ProjectionY();
 h2humiyear_py->Draw();
 c1year_hist->SaveAs("year_hist.png");
 c1year_hist->SaveAs("year_hist.pdf");

 fopen.Close();




 TFile *fopen_w;
 // gROOT->ProcessLine(".L lhcbStyle.C");
 // lhcbStyle();
  
 
 fopen_w = new TFile("soaplog_weather.root","OPEN");



 TCanvas* c2 = new TCanvas("c2", "Weather Conditions", 800, 800);
 gStyle->SetOptStat(0);
 c2->Divide(1,2);
 c2_1->cd();
 TH2F * h3temp = new TH2F("h3temp","Outdoor Temperature (#circC)",nlines,unixtime-2419200,unixtime,100,-10.0,40.0);
 T_w->Project("h3temp","temperature_w:unixtime_w");
 h3temp->GetXaxis()->SetTimeDisplay(1);
 h3temp->GetXaxis()->SetTimeOffset(0,"gmt");
 h3temp->GetXaxis()->SetTimeFormat("%d/%m");
 // h3temp->GetXaxis()->SetTitle("Date");
 //h3temp->GetYaxis()->SetTitle("Temperature (#circC)");
 Double_t meantemp = h3temp->GetMean(2);
 TLine * lmeantemp = new TLine(unixtime-2419200,meantemp,unixtime,meantemp);
 TLine * lmeantempup = new TLine(unixtime-2419200,meantemp+0.5,unixtime,meantemp+0.5);
 TLine * lmeantempdown = new TLine(unixtime-2419200,meantemp-0.5,unixtime,meantemp-0.5);
 lmeantemp->SetLineColor(kBlue);lmeantempup->SetLineColor(kRed);lmeantempdown->SetLineColor(kRed);
 h3temp->SetMarkerStyle(kFullDotMedium);

 h3temp->Draw("");
TH3F * h3tempcopy = (TH3F*)h3temp->Clone("h3tempcopy");
 h3tempcopy->GetXaxis()->SetTimeDisplay(1);
h3tempcopy->GetXaxis()->SetTimeOffset(0,"gmt");
 h3tempcopy->GetXaxis()->SetTimeFormat("%Y");
 h3tempcopy->GetXaxis()->SetLabelOffset(0.05);
 TPad * c2_1copy = (TPad*)c2_1->Clone("c2_1copy");
 c2_1copy->SetFillStyle(4000);
 c2->cd();
 c2_1copy->Draw();
 c2_1copy->cd();
 h3tempcopy->Draw("AXIS");
 
 h3temp->Draw("same");//lmeantemp->Draw("same");lmeantempup->Draw("same");lmeantempdown->Draw("same");

 c2_2->cd();
 TH2F * h3humi = new TH2F("h3humi","Outdoor Rel. Humidity (\%)",nlines,unixtime-2419200,unixtime,101,-0.5,100.5);
 T_w->Project("h3humi","humidity_w:unixtime_w");
 h3humi->GetXaxis()->SetTimeDisplay(1);
 h3humi->GetXaxis()->SetTimeOffset(0,"gmt");
 h3humi->GetXaxis()->SetTimeFormat("%d/%m");
 h3humi->GetXaxis()->SetTitle("Date");
 h3humi->GetXaxis()->SetLabelOffset(0.01);
 h3humi->SetMarkerStyle(kFullDotMedium);
 h3humi->Draw("");

 TH3F * h3humicopy = (TH3F*)h3humi->Clone("h3humicopy");
 h3humicopy->GetXaxis()->SetTimeDisplay(1);
 h3humicopy->GetXaxis()->SetTimeOffset(0,"gmt");
 h3humicopy->GetXaxis()->SetTimeFormat("%Y");
 h3humicopy->GetXaxis()->SetLabelOffset(0.05);
 TPad * c2_2copy = (TPad*)c2_2->Clone("c2_2copy");
 c2_2copy->SetFillStyle(4000);
 c2->cd();
 c2_2copy->Draw();
 c2_2copy->cd();
 h3humicopy->Draw("AXIS");
 h3humi->Draw("same");

 c2->Update();
 c2->SaveAs("weatherconditions2.png");
 c2->SaveAs("weatherconditions2.pdf");
   
}
