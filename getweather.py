#!/usr/bin/python

import urllib2 
import json 
import watercontent
import re

f = urllib2.urlopen('http://api.wunderground.com/api/82f17a302c2a3fef/geolookup/conditions/forecast/q/pws:IBADENWR302.json') 
json_string = f.read() 
parsed_json = json.loads(json_string) 
location = parsed_json['location']['city'] 
temp_c = parsed_json['current_observation']['temp_c']
humid = parsed_json['current_observation']['relative_humidity']
humid = humid[:-1]
humid = re.sub("[^a-z0-9]+","", humid, flags=re.IGNORECASE)
if not humid:
    humid = "-1"
local_epoch = str(parsed_json['current_observation']['local_epoch'])
obs_epoch = str(parsed_json['current_observation']['observation_epoch'])
local_time  = str(parsed_json['current_observation']['local_time_rfc822'])



weather = str(parsed_json['current_observation']['weather'])
weather = re.sub("[^a-z0-9]+","", weather, flags=re.IGNORECASE)
weather.replace(" ", "")
if not weather:
    weather = "NULL"

precip = parsed_json['current_observation']['precip_today_metric']
precip = re.sub("[^a-z0-9]+","", precip, flags=re.IGNORECASE)
if not precip:
    precip = "0"

pressure_mb = parsed_json['current_observation']['pressure_mb']
#print "Current temperature in %s is: %s degrees Celcius" % (location, temp_c) 
#print "The relative humidity is: %s percent" % (humid) 
#print "The time of observation was %s " % (local_epoch)

h2oppm = watercontent.h2o_ppm(float(humid),float(temp_c))
h2ogm3 = watercontent.h2o_grams_per_cubic_meter(float(humid),float(temp_c))

with open("/home/soap/public_html/soap/heidelbergweather.txt", "a+") as myfile:
    myfile.write("%s %s %s %s %.1f %.3f %s %s (unixtime/temp/relH/press/ppm/gm^-3/precip) %s \n" % (local_epoch, temp_c, humid, pressure_mb, h2oppm, h2ogm3, precip, obs_epoch, weather))
f.close()
myfile.close()
