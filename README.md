This is code is mostly hacked together and is functional. Nothing guarantees it is pretty or the best way to do it. You need a working web server (Apache, etc.)

this code uses the nusoap library. Theoretically it should work for other SOAP libraries, but I haven't checked. 
(/usr/share/php/nusoap/nusoap.php)

Explanation of the files:

server2.php is the SOAP protocol server. The Comet T7511 sensor needs the web location of this file in its settings. The data is written under soaplog/soaplog2.log.

updatesoap.php is the web interface that updates the plots and generates the root files on the server. 

plotsoapdata2.C is the ROOT script that generates the data plots (pdf and png) and root files, soaplog.root and soaplog_weather.root. 

plotsoapdata.sh is the bash script that runs the root script. Is called by updatesoap.php.

getweather.py is a Python script that uses JSON the get the weather report from a local weather station. 
Use a cron job to get the weather in 15 - 60 minute intervals. As you want. The station is hardcoded for Heidelberg at the moment.
The data is written to heidelbergweather.txt

watercontent.py is used to calculate the absolute humidity by getweather.py.







