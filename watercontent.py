""" Python code for calculating dew point and absolute moisture content
from input humidity and temperature

By Joe Ellsworth
(c) Aug 2010 - Free Use for all, No warranty, No promises, No claims
 You may use or embed this as desired with no payment to me.
 joexdobs@gmail.com
"""

import math

TEMP_K_C_OFFSET = 237.3
UCONST1 = 0.66077
CUBIC_FOOT_PER_CUBIC_METER = 35.3146667
CUBIC_INCH_PER_CUBIC_FOOT = 1728
CUBIC_INCH_PER_CUBIC_YARD = 46656
CUBIC_INCH_PER_GALLON = 230.9851624
CUBIC_FOOT_PER_GALLON = 0.133671969
GALLONS_PER_CUBIC_FOOT = 7.481
GALLONS_PER_CUBIC_METER = 264.17
LITERS_PER_GALLON = 3.7854118
GRAMS_PER_GALLON = 3778.4
GRAMS_PER_POUND = 453.5924
GRAMS_PER_OUNCE = 28.3495231


def C2K(temp_c):
  return temp_c + TEMP_K_C_OFFSET

def C2F(temp_c):
  return ((temp_c * 9.0)/5.0) + 32.0
      
def F2C(temp_f):
  return ((temp_f - 32.0) * 5.0) / 9.0

def gram2ounce(grams):
  return grams / GRAMS_PER_OUNCE

def ounce2gram(ounces):
  return ounces * GRAMS_PER_OUNCE

def gram2gallon(grams):
  return grams / GRAMS_PER_GALLON

def gallon2gram(gallons):
  return gallons * GRAMS_PER_GALLON

def cubicfoot2cubicmeter(feet):
  return feet / CUBIC_FOOT_PER_CUBIC_METER

def cubicmeter2cubicfoot(meters):
  return meters * CUBIC_FOOT_PER_CUBIC_METER



""" Based in input releative humidity and Temperature
convert a given humidity and temperature in C to a dew point
in C.   rel_humid= to the relative humidity as eg 90.5 = 90.5%
temp is temperature C.
 Dew Point formula - University of Arizon -
    http://cals.arizona.edu/azmet/dewpoint.html
"""
def calc_dew_c(rel_humid, temp_c):
  l = math.log(rel_humid / 100.0)
  m = 17.27 * temp_c
  n = C2K(temp_c)
  b = (l + (m / n)) / 17.27
  dew_c = (TEMP_K_C_OFFSET * b) / (1 - b)
  return dew_c

""" Another formula.  This one seems to consistently over state
results by about 0.1C as compared to the previous formula.  Neither
formula duplicates the results from our excel formula because the python
log returns slightly different results than the excel ln() funciton.  The
difference is normally less than 0.2C.
"""
def calc_dew_c_v1(rel_humid, temp_c):
  es0 = 6.11
  c1  = 17.27
  c2  = 237.7
  rhp = rel_humid / 100.0
  temp_k = C2K(temp_c)
  sat_vapor_pressure_es = es0 * 10 ** (7.5 * temp_c / temp_k)
  curr_pressure_e = rhp * sat_vapor_pressure_es
  temp_dew_c = (-430.22+237.7* math.log(curr_pressure_e)) / (0 - math.log(curr_pressure_e)+19.08)
  #print "rhp=%f  sat_vapor_pressure_es=%f curr_pressue_e=%f  temp_dew_c=%f" % (
  #  rhp,sat_vapor_pressure_es,curr_pressure_e, temp_dew_c)
  return temp_dew_c

"""Return parts per million of H2O in
air at specified relative humidity
and temp_c"""
def h2o_ppm(rel_humid, temp_c):
  CA = 8.1332
  CB = 1762.39
  CC = 235.66
  dew_c = calc_dew_c(rel_humid, temp_c)
  curr_temp_part_pressure = CA - (CB / (temp_c + CC))
  amb_part_press_mm_hg =  10 ** curr_temp_part_pressure
  dew_temp_part_pressure = CA - (CB / (dew_c + CC))
  dew_temp_part_press_mm_hg = 10 ** dew_temp_part_pressure
  water_ppm = (dew_temp_part_press_mm_hg / 760.0) * 1000000
  return water_ppm
    
def h2o_grams_per_cubic_meter(rel_humid, temp_c):
  water_ppm = h2o_ppm(rel_humid, temp_c)
  water_gram_per_cubic_meter = water_ppm * 0.001 * 18.0 / 22.4
  return water_gram_per_cubic_meter

def h2o_ounce_per_cubic_foot(rel_humid, temp_c):
  gpcm = h2o_grams_per_cubic_meter(rel_humid, temp_c)
  ounce_per_cubic_meter = gram2ounce(gpcm)
  ounce_per_cubic_foot = ounce_per_cubic_meter / CUBIC_FOOT_PER_CUBIC_METER
  return ounce_per_cubic_foot

